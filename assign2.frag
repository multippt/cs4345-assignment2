//============================================================
// STUDENT NAME: Kwan Yong Kang Nicholas
// MATRIC NO.  : A0080933E
// NUS EMAIL   : a0080933@nus.edu.sg
// COMMENTS TO GRADER:
// 
//
// ============================================================
//
// FILE: assign2.frag

// The GL_EXT_gpu_shader4 extension extends GLSL 1.10 with 
// 32-bit integer (int) representation, integer bitwise operators, 
// and the modulus operator (%).


#extension GL_EXT_gpu_shader4 : require

#extension GL_ARB_texture_rectangle : require


uniform sampler2DRect InputTex;  // The input texture.

uniform int TexWidth;   // Always an even number.
uniform int TexHeight;

uniform int PassCount;  // For the very first pass, PassCount == 0.


void main()
{
    float P1 = texture2DRect(InputTex, gl_FragCoord.xy).a;
    float P2;

    if (PassCount % 2 == 0)  // PassCount is Even.
    {
        int column = int(gl_FragCoord.x);
		int offset = 1; // +1 if P1 is LHS (even), -1 if P1 is RHS (odd)
		if (column % 2 == 1) {
			offset = -1;
		}
		int secondColumn = column + offset;
		
		// If TexWidth is even, P2 will always reside in the same row as P1
		P2 = texture2DRect(InputTex, vec2(float(secondColumn) + 0.5, gl_FragCoord.y)).a;
		
		// If P1 is LHS, then we compare P1 < P2, otherwise we compare P2 < P1 (by negating it).
		gl_FragColor = (offset * P1 < offset * P2) ? P1 : P2;
    }
    else  // PassCount is Odd.
    {
        int row = int(gl_FragCoord.y);
        int column = int(gl_FragCoord.x);
        int index1D = row * TexWidth + column;

		int offset = -1; // +1 if P1 is LHS (odd), -1 if P1 is RHS (even)
		if (index1D % 2 == 1) {
			offset = 1;
		}
		int secondIndex = index1D + offset;
		
		// Initialize to P1 position
		int secondColumn = column;
		int secondRow = row;
		// Find P2 2D position (if P1 is the first or last element, P2 = P1 so that it will compare to itself)
		if (0 < index1D && index1D < (TexWidth * TexHeight - 1)) {
			secondColumn = secondIndex % TexWidth;
			secondRow = int(secondIndex / TexWidth);
		}
		
		P2 = texture2DRect(InputTex, vec2(float(secondColumn) + 0.5, float(secondRow) + 0.5)).a;
		
		// If P1 is LHS, then we compare P1 < P2, otherwise we compare P2 < P1.
		gl_FragColor = (offset * P1 < offset * P2) ? P1 : P2;
    }
}
